from flask import Blueprint
from mariadb import ConnectionPool


def create_blueprint(dbpool: ConnectionPool) -> Blueprint:
    bp = Blueprint('users', __name__, url_prefix='/users')

    @bp.route('/<int:key>/recipes')
    def user_login(key: int):
        pass

    return bp
