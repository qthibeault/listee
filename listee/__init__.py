from flask import Flask

import listee.db as db
import listee.users as users
import listee.recipes as recipes
import listee.lists as lists


db_config = db.get_config()
if db_config.user is None or db_config.password is None:
    raise RuntimeError('Database user or password cannot be None')

db_pool = db.get_pool(db_config)

app = Flask(__name__)
app.register_blueprint(users.create_blueprint(db_pool))
app.register_blueprint(recipes.create_blueprint(db_pool))
app.register_blueprint(lists.create_blueprint(db_pool))
