import os
import time
from collections import namedtuple

import mariadb

DbConfig = namedtuple('DbConfig', ['host', 'port', 'user', 'password', 'database'])


def get_config() -> DbConfig:
    db_host = os.environ.get('DB_HOST')
    if db_host is None:
        db_host = 'localhost'

    db_port = os.environ.get('DB_PORT')
    if db_port is None:
        db_port = 3306

    db_name = os.environ.get('DB_NAME')
    if db_name is None:
        db_name = 'listee'

    db_user = os.environ.get('DB_USER')
    db_pass = os.environ.get('DB_PASS')

    return DbConfig(db_host, db_port, db_user, db_pass, db_name)


def get_pool(db_config: DbConfig, wait_time=0.05) -> mariadb.ConnectionPool:
    while True:
        try:
            return mariadb.ConnectionPool(
                pool_name='pool_1',
                host=db_config.host,
                port=db_config.port,
                user=db_config.user,
                password=db_config.password,
                database=db_config.database
            )
        except mariadb.Error:
            time.sleep(wait_time)
            wait_time = wait_time * 2
