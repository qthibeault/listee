from flask import Blueprint, request
from mariadb import ConnectionPool


def create_blueprint(db_pool: ConnectionPool) -> Blueprint:
    bp = Blueprint('lists', __name__, url_prefix='/lists')

    @bp.route('/generate', methods=['POST'])
    def generate_list():
        pass

    return bp
