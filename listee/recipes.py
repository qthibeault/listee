from flask import Blueprint
from mariadb import ConnectionPool


def create_blueprint(db_config: ConnectionPool) -> Blueprint:
    bp = Blueprint('recipes', __name__, url_prefix='/recipes')

    @bp.route('/<int:key>')
    def read_recipe(key: int):
        pass

    return bp
